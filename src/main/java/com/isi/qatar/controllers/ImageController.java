package com.isi.qatar.controllers;

import com.google.common.io.ByteSource;
import com.isi.qatar.entities.Image;
import com.isi.qatar.repository.ImageRepository;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@RestController
@RequestMapping("/v1/Images")
@CrossOrigin(origins = "*")
public class ImageController {
    private final ImageRepository imageRepository;


    public ImageController(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/")
    public ResponseEntity uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
        System.out.println("Original Image Byte Size - " + file.getBytes().length);
        Image img = new Image(Long.valueOf(0),file.getOriginalFilename(), file.getContentType(), compressBytes(file.getBytes()));


        Image image = imageRepository.save(img);
            return ResponseEntity.ok(image);
    }


    @GetMapping(path = { "/{id}" })
    public Image getImage(@PathVariable("id") Long id) throws IOException {

        final Optional<Image> retrievedImage = imageRepository.findById(id);

        Image img = new Image(retrievedImage.get().getId(),retrievedImage.get().getNom(), retrievedImage.get().getType(), decompressBytes(retrievedImage.get().getImageByte()));

        return img;
    }

    @GetMapping(value = "/Low/{id}")
    public ResponseEntity<InputStreamResource>  getImageLow(@PathVariable("id") Long id) throws IOException {
        final Optional<Image> retrievedImage = imageRepository.findById(id);
if (retrievedImage.isPresent()) {
    Image img = new Image(retrievedImage.get().getId(), retrievedImage.get().getNom(), retrievedImage.get().getType(), decompressBytes(retrievedImage.get().getImageByte()));
    System.out.println(img.getImageByte().length);
    BufferedImage imgs = ImageIO.read( new ByteArrayInputStream(img.getImageByte()));
    System.out.println(imgs);

    InputStream targetStream = resize(imgs,100,100);

    return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType("image/jpeg"))
            .body(new InputStreamResource(targetStream));
}else{
    return ResponseEntity.badRequest().build();
}
    }


    @GetMapping(value = "/High/{id}")
    public ResponseEntity<InputStreamResource>  getImageHigh(@PathVariable("id") Long id) throws IOException {
        final Optional<Image> retrievedImage = imageRepository.findById(id);
        if (retrievedImage.isPresent()) {
            Image img = new Image(retrievedImage.get().getId(), retrievedImage.get().getNom(), retrievedImage.get().getType(), decompressBytes(retrievedImage.get().getImageByte()));
            InputStream targetStream = ByteSource.wrap(img.getImageByte()).openStream();
            return ResponseEntity.ok()
                    .contentLength(img.getImageByte().length)
                    .contentType(MediaType.parseMediaType(img.getType()))
                    .body(new InputStreamResource(targetStream));
        }else{
            return ResponseEntity.badRequest().build();
        }
    }



    // compress the image bytes before storing it in the database
    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
                e.printStackTrace();
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

        return outputStream.toByteArray();
    }

    // uncompress the image bytes before returning it to the angular application
    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException ioe) {
        } catch (DataFormatException e) {
        }
        return outputStream.toByteArray();
    }

    private InputStream resize(BufferedImage img, int width, int height) throws IOException {

        double d = (double) img.getHeight() / ((double)img.getWidth()/width);
        System.out.println( d);
        java.awt.Image tmp = img.getScaledInstance(width,  img.getHeight()/ (img.getWidth()/width), java.awt.Image.SCALE_SMOOTH);
        BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g2d = resized.createGraphics();
        g2d.drawImage(tmp, 0, 0, null);
        g2d.dispose();

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(resized, "png", os);                          // Passing: ​(RenderedImage im, String formatName, OutputStream output)
        InputStream is = new ByteArrayInputStream(os.toByteArray());
        return is;
    }



}
