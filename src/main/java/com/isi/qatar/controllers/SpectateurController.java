package com.isi.qatar.controllers;


import com.isi.qatar.entities.Spectateurs;
import com.isi.qatar.entities.personne.Personne;
import com.isi.qatar.entities.personne.Role;
import com.isi.qatar.repository.SpectateursRepository;
import com.isi.qatar.repository.StadeRepository;
import com.isi.qatar.repository.personne.PersonneRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@RestController
@RequestMapping("/v1/Spectateurs")
@CrossOrigin(origins = "*")

public class SpectateurController {


    private final SpectateursRepository spectateurRepository;
    private PersonneRepository personneRepository;

    public SpectateurController(SpectateursRepository spectateurRepository, PersonneRepository personneRepository) {
        this.spectateurRepository = spectateurRepository;
        this.personneRepository = personneRepository;
    }


    @GetMapping("/")
    public ResponseEntity findAll(HttpServletRequest request){
      Personne personne=   personneRepository.findByUsername(request.getRemoteUser());
      if(personne.getRole().get(0) == Role.ROLE_ADMIN){
          return ResponseEntity.ok(spectateurRepository.findAll());
      }else{
          return ResponseEntity.ok(spectateurRepository.findAllBySpectateur(personne));
      }
    }

    @GetMapping("/{SpectateursId}")
    public ResponseEntity findSpectateursById(@PathVariable(name = "SpectateursId") Long spectateurId){
        if(spectateurId==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Spectateurs> spectateur = spectateurRepository.findById(spectateurId);

        if(spectateur.isPresent()){
            return ResponseEntity.ok(spectateur);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/")
    public ResponseEntity createSpectateurs (@RequestBody Spectateurs spectateurBody){
        if (spectateurBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Spectateurs> spectateur= spectateurRepository.findById(spectateurBody.getCodeTicket());
if(spectateurRepository.countAllByPartie(spectateurBody.getPartie())>=spectateurBody.getPartie().getStade().getCapacite()){
    return ResponseEntity.badRequest().body("Stade Overflow!");
}

        if (!spectateur.isPresent()){
            Spectateurs createSpectateurs = spectateurRepository.save(spectateurBody);
            return ResponseEntity.ok(createSpectateurs);
        }
        return ResponseEntity.badRequest().body("Spectateurs exist");
    }

    @DeleteMapping("/{spectateurId}")
    public  ResponseEntity deleteSpectateurs (@PathVariable(name = "spectateurId") Long spectateurNumber){
        if (spectateurNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Spectateurs> spectateur= spectateurRepository.findById(spectateurNumber);

        if (spectateur.isPresent()){
            spectateurRepository.deleteById(spectateurNumber);
            return ResponseEntity.ok().body(spectateur);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updateSpectateurs (@RequestBody Spectateurs spectateurBody){
        if (spectateurBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Spectateurs> spectateur= spectateurRepository.findById(spectateurBody.getCodeTicket());

        if (spectateur.isPresent()){
            Spectateurs createSpectateurs = spectateurRepository.save(spectateurBody);
            return ResponseEntity.ok(createSpectateurs);
        }
        return ResponseEntity.notFound().build();




    }

}
