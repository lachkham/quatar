package com.isi.qatar.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.Equipe;
import com.isi.qatar.entities.personne.View;
import com.isi.qatar.repository.EquipeRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/Equipe")
@CrossOrigin(origins = "*")

public class EquipeController {

    private final EquipeRepository equipeRepository;

    public EquipeController(EquipeRepository repository) {
        this.equipeRepository = repository;
    }


    @GetMapping("/")
    @JsonView(View.Summary.class)
    public ResponseEntity findAll(){
        return ResponseEntity.ok(equipeRepository.findAll());
    }

    @GetMapping("/{EquipeId}")
    public ResponseEntity findEquipeById(@PathVariable(name = "EquipeId") Long equipeId){
        if(equipeId==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Equipe> equipe = equipeRepository.findById(equipeId);

        if(equipe.isPresent()){
            return ResponseEntity.ok(equipe);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/")
    public ResponseEntity createEquipe (@RequestBody Equipe equipeBody){
        if (equipeBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Equipe> equipe= equipeRepository.findById(equipeBody.getIdEquipe());

        if (!equipe.isPresent()){
            Equipe createEquipe = equipeRepository.save(equipeBody);
            return ResponseEntity.ok(createEquipe);
        }
        return ResponseEntity.badRequest().body("Equipe exist");
    }

    @DeleteMapping("/{equipeId}")
    public  ResponseEntity deleteEquipe (@PathVariable(name = "equipeId") Long equipeNumber){
        if (equipeNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Equipe> equipe= equipeRepository.findById(equipeNumber);

        if (equipe.isPresent()){
            equipeRepository.deleteById(equipeNumber);
            return ResponseEntity.ok().body(equipe);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updateEquipe (@RequestBody Equipe equipeBody){
        if (equipeBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Equipe> equipe= equipeRepository.findById(equipeBody.getIdEquipe());

        if (equipe.isPresent()){
            Equipe createEquipe = equipeRepository.save(equipeBody);
            return ResponseEntity.ok(createEquipe);
        }
        return ResponseEntity.notFound().build();




    }
}
