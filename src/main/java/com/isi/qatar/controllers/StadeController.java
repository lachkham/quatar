package com.isi.qatar.controllers;



import com.isi.qatar.entities.Stade;
import com.isi.qatar.repository.StadeRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/Stade")
@CrossOrigin(origins = "*")

public class StadeController {

    private final StadeRepository stadeRepository;

    public StadeController(StadeRepository repository) {
        this.stadeRepository = repository;
    }


    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(stadeRepository.findAll());
    }

    @GetMapping("/{StadeId}")
    public ResponseEntity findStadeById(@PathVariable(name = "StadeId") Long stadeId){
        if(stadeId==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Stade> stade = stadeRepository.findById(stadeId);

        if(stade.isPresent()){
            return ResponseEntity.ok(stade);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/")
    public ResponseEntity createStade (@RequestBody Stade stadeBody){
        if (stadeBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Stade> stade= stadeRepository.findById(stadeBody.getIdStade());

        if (!stade.isPresent()){
            Stade createStade = stadeRepository.save(stadeBody);
            return ResponseEntity.ok(createStade);
        }
        return ResponseEntity.badRequest().body("Stade exist");
    }

    @DeleteMapping("/{stadeId}")
    public  ResponseEntity deleteStade (@PathVariable(name = "stadeId") Long stadeNumber){
        if (stadeNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Stade> stade= stadeRepository.findById(stadeNumber);

        if (stade.isPresent()){
            stadeRepository.deleteById(stadeNumber);
            return ResponseEntity.ok().body(stade);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updateStade (@RequestBody Stade stadeBody){
        if (stadeBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Stade> stade= stadeRepository.findById(stadeBody.getIdStade());

        if (stade.isPresent()){
            Stade createStade = stadeRepository.save(stadeBody);
            return ResponseEntity.ok(createStade);
        }
        return ResponseEntity.notFound().build();




    }

}
