package com.isi.qatar.controllers;

import com.isi.qatar.entities.Partie;
import com.isi.qatar.repository.PartieRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/Partie")
@CrossOrigin(origins = "*")

public class PartieController {

    private final PartieRepository partieRepository;

    public PartieController(PartieRepository repository) {
        this.partieRepository = repository;
    }


    @GetMapping("/")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(partieRepository.findAll());
    }

    @GetMapping("/{PartieId}")
    public ResponseEntity findPartieById(@PathVariable(name = "PartieId") Long partieId){
        if(partieId==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Partie> partie = partieRepository.findById(partieId);

        if(partie.isPresent()){
            return ResponseEntity.ok(partie);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/")
    public ResponseEntity createPartie (@RequestBody Partie partieBody){
        if (partieBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Partie> partie= partieRepository.findById(partieBody.getIdPartie());

        if (!partie.isPresent()){
            Partie createPartie = partieRepository.save(partieBody);
            return ResponseEntity.ok(createPartie);
        }
        return ResponseEntity.badRequest().body("Partie exist");
    }

    @DeleteMapping("/{partieId}")
    public  ResponseEntity deletePartie (@PathVariable(name = "partieId") Long partieNumber){
        if (partieNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Partie> partie= partieRepository.findById(partieNumber);

        if (partie.isPresent()){
            partieRepository.deleteById(partieNumber);
            return ResponseEntity.ok().body(partie);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updatePartie (@RequestBody Partie partieBody){
        if (partieBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Partie> partie= partieRepository.findById(partieBody.getIdPartie());

        if (partie.isPresent()){
            Partie createPartie = partieRepository.save(partieBody);
            return ResponseEntity.ok(createPartie);
        }
        return ResponseEntity.notFound().build();




    }
}
