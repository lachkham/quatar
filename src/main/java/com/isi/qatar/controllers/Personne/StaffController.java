package com.isi.qatar.controllers.Personne;

import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.personne.Staff;
import com.isi.qatar.entities.personne.View;
import com.isi.qatar.repository.personne.StaffRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/v1/Staff")
@CrossOrigin(origins = "*")
public class StaffController {


    private final StaffRepository staffRepository;

    public StaffController(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }


    @GetMapping("/")
    @JsonView(View.Summary.class)
    public ResponseEntity findAll(){
        return ResponseEntity.ok(staffRepository.findAll());
    }

    @JsonView(View.Summary.class)
    @GetMapping("/equipe/{IdEquipe}")
    public ResponseEntity findAll(@PathVariable(name = "IdEquipe") Long idEquipe) {
        return ResponseEntity.ok(staffRepository.findAllByEquipe_IdEquipe(idEquipe));
    }

    @GetMapping("/{StaffId}")
    public ResponseEntity findStaffById(@PathVariable(name = "StaffId") Long staffId){
        if(staffId==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Staff> staff = staffRepository.findById(staffId);

        if(staff.isPresent()){
            return ResponseEntity.ok(staff);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/")
    public ResponseEntity createStaff (@RequestBody Staff staffBody){
        if (staffBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Staff> staff= staffRepository.findById(staffBody.getCin());

        if (!staff.isPresent()){
            Staff createStaff = staffRepository.save(staffBody);
            return ResponseEntity.ok(createStaff);
        }
        return ResponseEntity.badRequest().body("Staff exist");
    }

    @DeleteMapping("/{staffId}")
    public  ResponseEntity deleteStaff (@PathVariable(name = "staffId") Long staffNumber){
        if (staffNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Staff> staff= staffRepository.findById(staffNumber);

        if (staff.isPresent()){
            staffRepository.deleteById(staffNumber);
            return ResponseEntity.ok().body(staff);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updateStaff (@RequestBody Staff staffBody){
        if (staffBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Staff> staff= staffRepository.findById(staffBody.getCin());

        if (staff.isPresent()){
            Staff createStaff = staffRepository.save(staffBody);
            return ResponseEntity.ok(createStaff);
        }
        return ResponseEntity.notFound().build();




    }
}
