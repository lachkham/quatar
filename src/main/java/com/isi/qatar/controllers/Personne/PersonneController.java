package com.isi.qatar.controllers.Personne;


import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.personne.Personne;
import com.isi.qatar.entities.personne.Role;
import com.isi.qatar.entities.personne.View;
import com.isi.qatar.repository.personne.PersonneRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/Personne")
@CrossOrigin(origins = "*")
public class PersonneController {

    private final PersonneRepository personneRepository;

    public PersonneController(PersonneRepository repository) {
        this.personneRepository = repository;
    }

  //  private BCryptPasswordEncoder bCryptPasswordEncoder;


    @GetMapping("/")
    @JsonView(View.Summary.class)
    public ResponseEntity findAll(){
        return ResponseEntity.ok(personneRepository.findAll());
    }

    @GetMapping("/user")
    @JsonView(View.Summary.class)
    public ResponseEntity WhoImI(HttpServletRequest request){
        return ResponseEntity.ok(personneRepository.findByUsername(request.getRemoteUser()));
    }

    @GetMapping("/{PersonneId}")
    public ResponseEntity findPersonneById(@PathVariable(name = "PersonneId") Long personneId){
        if(personneId==null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Personne> personne = personneRepository.findById(personneId);

        if(personne.isPresent()){
            return ResponseEntity.ok(personne);
        }else{
            return  ResponseEntity.notFound().build();
        }
    }
    @CrossOrigin(origins = "*")
    @PostMapping("/Inscription")
    public ResponseEntity createPersonne (@RequestBody Personne personneBody){
        if (personneBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Personne> personne= personneRepository.findById(personneBody.getCin());
        Optional<Personne> personne2= Optional.ofNullable(personneRepository.findByUsername(personneBody.getUsername()));

        if (!personne.isPresent() && !personne2.isPresent()){
            personne= Optional.ofNullable(personneRepository.findByUsername(personneBody.getUsername()));
            if(!personne.isPresent()){
                if(personneBody.getPassword()!=null){
                    personneBody.setPassword(bCryptPasswordEncoder().encode(personneBody.getPassword()));

                }
                List<Role> role = personneBody.getRole();
                    role.add(Role.ROLE_CLIENT);
                personneBody.setRole(role);
            Personne createPersonne = personneRepository.save(personneBody);
            return ResponseEntity.ok(createPersonne);
            }
        }
        return ResponseEntity.badRequest().body("CIN/Username exist");
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/addAdmin")
    public ResponseEntity createPersonneAdmin (@RequestBody Personne personneBody){
        if (personneBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }

        Optional<Personne> personne= personneRepository.findById(personneBody.getCin());

        if (!personne.isPresent()){
            personne= Optional.ofNullable(personneRepository.findByUsername(personneBody.getUsername()));
            if(!personne.isPresent()){
                if(personneBody.getPassword()!=null){
                    personneBody.setPassword(bCryptPasswordEncoder().encode(personneBody.getPassword()));

                }
                List<Role> role = personneBody.getRole();
                role.add(Role.ROLE_ADMIN);
                personneBody.setRole(role);
                Personne createPersonne = personneRepository.save(personneBody);
                return ResponseEntity.ok(createPersonne);}
        }
        return ResponseEntity.badRequest().body("CIN/Username exist");
    }

    @DeleteMapping("/{personneId}")
    public  ResponseEntity deletePersonne (@PathVariable(name = "personneId") Long personneNumber){
        if (personneNumber ==  null){
            return ResponseEntity.badRequest().body("Empty parameter");
        }

        Optional<Personne> personne= personneRepository.findById(personneNumber);

        if (personne.isPresent()){
            personneRepository.deleteById(personneNumber);
            return ResponseEntity.ok().body(personne);
        }
        return ResponseEntity.notFound().build();

    }

    @PutMapping("/")
    public ResponseEntity updatePersonne (@RequestBody Personne personneBody){
        if (personneBody==null){
            return  ResponseEntity.badRequest().body("Empty Request Body");
        }


        Optional<Personne> personne= personneRepository.findById(personneBody.getCin());

        if (personne.isPresent()){
            List<Role> role = personneBody.getRole();



            personneBody.setRole(role);
            Personne createPersonne = personneRepository.save(personneBody);

            return ResponseEntity.ok(createPersonne);
        }
        return ResponseEntity.notFound().build();




    }
    @Bean
    public PasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
