package com.isi.qatar.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.personne.View;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
@Data
@Entity
public class Equipe implements Serializable {
    @Id
    @GeneratedValue
    @JsonView(View.Summary.class)
    private Long idEquipe;
    @JsonView(View.Summary.class)
    private String pays;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonView(View.Summary.class)
    private Image drapeau;

}
