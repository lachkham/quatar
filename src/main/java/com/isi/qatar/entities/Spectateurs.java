package com.isi.qatar.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.personne.Personne;
import com.isi.qatar.entities.personne.View;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Entity
@Data
public class Spectateurs implements Serializable {
    @Id
    @GeneratedValue
    @JsonView(View.Summary.class)
    private Long codeTicket;

    @ManyToOne
    @JsonView(View.Summary.class)
    private Personne spectateur;
    @ManyToOne
    @JsonView(View.Summary.class)
    private Partie partie;
}
