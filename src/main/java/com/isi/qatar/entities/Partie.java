package com.isi.qatar.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.personne.View;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Date;

@Entity
@Data
public class Partie implements Serializable {
    @Id
    @GeneratedValue
    @JsonView(View.Summary.class)
    private Long idPartie;
    @ManyToOne
    @JsonView(View.Summary.class)
    private Equipe eq1;
   @ManyToOne
   @JsonView(View.Summary.class)
    private Equipe eq2;
    @ManyToOne
    @JsonView(View.Summary.class)
    private Stade stade;
    @JsonView(View.Summary.class)
    private int scoreEq1;
    @JsonView(View.Summary.class)
    private int scoreEq2;
    @JsonView(View.Summary.class)
    private Long date;
    @JsonView(View.Summary.class)
    private String tours;

}
