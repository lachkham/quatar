package com.isi.qatar.entities;

import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.personne.View;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
@Entity
@Data
public class Stade implements Serializable {
    @Id
    @GeneratedValue
    @JsonView(View.Summary.class)
    private Long    idStade;
    @JsonView(View.Summary.class)
    private String  nomStade;
    @JsonView(View.Summary.class)
    private int     capacite;

}
