package com.isi.qatar.entities.personne;


import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.Image;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Data

public class Personne implements Serializable {
    @Id
    @JsonView(View.Summary.class)
    private Long cin;
    @JsonView(View.Summary.class)
    private String nom;
    @JsonView(View.Summary.class)
    private String prenom;
    @JsonView(View.Summary.class)
    private Long dateNaiss;

   @ManyToOne(fetch = FetchType.EAGER)
   @JsonView(View.Summary.class)
   private Image image;



   //security
  //  @Column(columnDefinition = "default 'Lachkham'")
   @JsonView(View.Summary.class)
   private String username;
  //  @Column(columnDefinition = "default ''")
   private String password;
   @ElementCollection(fetch = FetchType.EAGER)
   @JsonView(View.Summary.class)
   List<Role> role;



}
