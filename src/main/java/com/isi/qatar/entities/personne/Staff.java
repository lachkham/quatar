package com.isi.qatar.entities.personne;

import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.Equipe;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.io.Serializable;
@Entity
@Data
@EqualsAndHashCode(callSuper=true)

public class Staff extends Personne implements Serializable {

    @ManyToOne
    @JsonView(View.Summary.class)
    private Equipe equipe;
    @JsonView(View.Summary.class)
    private String fonction;
}
