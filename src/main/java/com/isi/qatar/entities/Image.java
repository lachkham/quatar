package com.isi.qatar.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.isi.qatar.entities.personne.Personne;
import com.isi.qatar.entities.personne.View;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Image implements Serializable {
    @Id
    @GeneratedValue
    @JsonView(View.Summary.class)
    private Long id;

    private String nom;
    private String type;

    @Column(name="image", length = 1000)
    private  byte[] imageByte;

}
