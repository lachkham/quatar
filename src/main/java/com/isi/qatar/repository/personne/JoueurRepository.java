package com.isi.qatar.repository.personne;

import com.isi.qatar.entities.personne.Joueur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JoueurRepository extends JpaRepository<Joueur,Long> {
    List<Joueur> findAllByEquipe_IdEquipe(Long idEquipe);
}
