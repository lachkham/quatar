package com.isi.qatar.repository.personne;

import com.isi.qatar.entities.personne.Joueur;
import com.isi.qatar.entities.personne.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonneRepository extends JpaRepository<Personne,Long> {
    Personne findByUsername(String username);
}
