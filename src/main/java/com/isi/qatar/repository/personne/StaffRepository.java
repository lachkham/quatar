package com.isi.qatar.repository.personne;

import com.isi.qatar.entities.personne.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaffRepository extends JpaRepository<Staff,Long> {
    List<Staff> findAllByEquipe_IdEquipe(Long idEquipe);
}
