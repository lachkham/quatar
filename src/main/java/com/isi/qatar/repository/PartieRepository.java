package com.isi.qatar.repository;

import com.isi.qatar.entities.Partie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PartieRepository extends JpaRepository<Partie,Long> {
}
