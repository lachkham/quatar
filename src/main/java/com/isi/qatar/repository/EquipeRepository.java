package com.isi.qatar.repository;

import com.isi.qatar.entities.Equipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipeRepository  extends JpaRepository<Equipe,Long> {
}
