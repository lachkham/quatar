package com.isi.qatar.repository;

import com.isi.qatar.entities.Partie;
import com.isi.qatar.entities.Spectateurs;
import com.isi.qatar.entities.personne.Personne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpectateursRepository extends JpaRepository<Spectateurs,Long> {
    List<Spectateurs> findAllBySpectateur(Personne personne);
    int countAllByPartie(Partie partie);
}
