package com.isi.qatar.security;

        import com.isi.qatar.entities.personne.Role;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.context.annotation.Bean;
        import org.springframework.context.annotation.Configuration;
        import org.springframework.http.HttpMethod;
        import org.springframework.security.authentication.AuthenticationManager;
        import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
        import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
        import org.springframework.security.config.annotation.web.builders.HttpSecurity;
        import org.springframework.security.config.annotation.web.builders.WebSecurity;
        import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
        import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
        import org.springframework.security.config.http.SessionCreationPolicy;
        import org.springframework.security.core.userdetails.UserDetailsService;
        import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
        import org.springframework.security.crypto.password.PasswordEncoder;
        import org.springframework.security.web.access.channel.ChannelProcessingFilter;
        import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
        import org.springframework.web.cors.CorsConfiguration;
        import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
        import org.springframework.web.filter.CorsFilter;

        import javax.servlet.*;
        import javax.servlet.http.HttpServletResponse;
        import java.io.IOException;
        import java.util.Arrays;
        import java.util.Collections;

@Configuration
@EnableWebSecurity

@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    private final UserDetailsService jwtUserDetailsService;

    private final JwtRequestFilter jwtRequestFilter;

    public WebSecurityConfig(JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint, UserDetailsService jwtUserDetailsService, JwtRequestFilter jwtRequestFilter) {
        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
        this.jwtUserDetailsService = jwtUserDetailsService;
        this.jwtRequestFilter = jwtRequestFilter;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        // configure AuthenticationManager so that it knows from where to load
        // user for matching credentials
        // Use BCryptPasswordEncoder
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public CorsFilter corsFilter() {
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        // Don't do this in production, use a proper list  of allowed origins
        config.setAllowedOrigins(Collections.singletonList("*"));
        config.setAllowedHeaders(Arrays.asList("Origin", "Content-Type", "Accept","Authorization"));
        config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"));
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Override
    public void configure(WebSecurity web) throws Exception {

        web.ignoring()
                .antMatchers("/v2/api-docs")//
                .antMatchers("/swagger-resources/**")//
                .antMatchers("/swagger-ui.html")//
                .antMatchers("/configuration/**")//
                .antMatchers("/webjars/**")//

                .antMatchers("/#/**")
                .antMatchers("/")
                .antMatchers("/#/home")
                .antMatchers("/#/connexion/**")
                .antMatchers("/#/dashboard/**")
                .antMatchers("/styles**")
                .antMatchers("/runtime**")
                .antMatchers("/polyfills**")
                .antMatchers("/favicon.ico**")
                .antMatchers("/main**")
                .antMatchers("/assets/**")





        ;

        web.ignoring().antMatchers("/files/**");
    }



    private static final String[] PUBLIC_Get_MATCHERS = {
            "/login",
            "/register",
            "/v1/Images/**",
            "/v1/Equipe/**",
            "/v1/Joueur/**",
            "/v1/Partie/**",

            "/logout",

            "/v1/Stade/**",
            "/v1/Staff/**",
         //   "/v1/Spectateurs/**",






    };
    private static final String[] PUBLIC_Post_MATCHERS = {
            "/login",
            "/v1/Personne/**",
            "/v1/Images/**",
            "/logout"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception{

        http
                .csrf().disable().cors().and()
                .httpBasic().and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,PUBLIC_Get_MATCHERS).permitAll()
                .antMatchers("/login", "/register","/v1/","checkSession","/v1/Images/**").permitAll()
                .antMatchers(HttpMethod.POST,PUBLIC_Post_MATCHERS).permitAll()
                .antMatchers(HttpMethod.POST,"/Spectateurs/**").hasRole(String.valueOf(Role.ROLE_CLIENT))

                .anyRequest().hasRole(String.valueOf(Role.ROLE_ADMIN)).and().
                // make sure we use stateless session; session won't be used to
                // store user's state.
                        exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        ;

        // Add a filter to validate the tokens with every request
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }


}

