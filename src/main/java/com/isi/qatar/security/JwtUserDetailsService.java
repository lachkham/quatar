package com.isi.qatar.security;

import java.util.ArrayList;

import com.isi.qatar.entities.personne.Personne;
import com.isi.qatar.repository.personne.PersonneRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private PersonneRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Personne user = userRepository.findByUsername(username);
        System.out.println("JwtUserDetailsService: "+user.getRole());
        if (user == null) {
            throw new UsernameNotFoundException("username '" + username + "' not found");
        }
        return org.springframework.security.core.userdetails.User//
                .withUsername(username)//
                .password(user.getPassword())//
                .authorities(user.getRole())//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)//
                .disabled(false)//
                .build();
    }

}

